package com.tracker.sekon.sekontracker.Entities.UnitTrack;

public class SpeedEntity {
    private float speed;
    private float averageSpeed;
    private float maxSpeed;

    public SpeedEntity(float speed, float averageSpeed, float maxSpeed) {
        this.speed = speed;
        this.averageSpeed = averageSpeed;
        this.maxSpeed = maxSpeed;
    }

    public float getSpeed() {
        return speed;
    }

    public float getAverageSpeed() {
        return averageSpeed;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

}
