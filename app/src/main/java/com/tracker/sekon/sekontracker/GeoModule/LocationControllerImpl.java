package com.tracker.sekon.sekontracker.GeoModule;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.tracker.sekon.sekontracker.App;
import com.tracker.sekon.sekontracker.Entities.UnitTrack.GeoPositionEntity;
import com.tracker.sekon.sekontracker.Mappers.GeoLocationMapper;

public class LocationControllerImpl {

    private static LocationControllerImpl instance;
    private LocationController locationController;
    private LocationListener locationListener;


    private LocationControllerImpl() {

    }

    public static LocationControllerImpl getInstance() {
        if(instance == null){
            instance = new LocationControllerImpl();
        }
        return instance;
    }


    public void setGeoListener(LocationController listener){
        this.locationController = listener;
        executeListener();
    }

    private void executeListener(){
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                locationController.getLocationData(new GeoLocationMapper().locationDataFromLocationMapper(location));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

}
