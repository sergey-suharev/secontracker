package com.tracker.sekon.sekontracker.Mappers;

import android.location.Location;

import com.tracker.sekon.sekontracker.Entities.UnitTrack.GeoPositionEntity;
import com.tracker.sekon.sekontracker.GeoModule.GeoLocationData;

public class GeoLocationMapper {

    public GeoLocationData locationDataFromLocationMapper(Location location){
        return new GeoLocationData(mapGeoPositionEntity(location), location.getSpeed());
    }

    private GeoPositionEntity mapGeoPositionEntity(Location location){

        return new GeoPositionEntity(location.getAltitude(),
                location.getLongitude(), location.getLatitude());
    }
}
