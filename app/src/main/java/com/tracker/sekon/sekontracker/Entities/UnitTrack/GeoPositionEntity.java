package com.tracker.sekon.sekontracker.Entities.UnitTrack;

public class GeoPositionEntity {

    private double altitude;
    private double longitude;
    private double latitude;

    public GeoPositionEntity(double altitude, double longitude, double latitude) {
        this.altitude = altitude;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
}
