package com.tracker.sekon.sekontracker.Fragments;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tracker.sekon.sekontracker.GeoModule.GeoLocationData;
import com.tracker.sekon.sekontracker.GeoModule.LocationController;
import com.tracker.sekon.sekontracker.GeoModule.LocationControllerImpl;
import com.tracker.sekon.sekontracker.R;
import com.tracker.sekon.sekontracker.ViewModels.CurrentTrackViewModel.CurrentTrackViewModel;

public class TestDisplayFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
