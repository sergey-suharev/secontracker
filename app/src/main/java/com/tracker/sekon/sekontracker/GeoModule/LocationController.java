package com.tracker.sekon.sekontracker.GeoModule;

public interface LocationController {

    public void getLocationData(GeoLocationData locationData);
}
