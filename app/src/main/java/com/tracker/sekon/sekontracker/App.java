package com.tracker.sekon.sekontracker;

public class App {

    private static String systemService;

    public static String getSystemService() {
        return systemService;
    }

    public static void setSystemService(String systemService) {
        App.systemService = systemService;
    }
}
