package com.tracker.sekon.sekontracker.ViewModels.CurrentTrackViewModel;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.view.ViewManager;

import com.tracker.sekon.sekontracker.Entities.UnitTrack.UnitTrackEntity;
import com.tracker.sekon.sekontracker.GeoModule.GeoLocationData;
import com.tracker.sekon.sekontracker.GeoModule.LocationController;
import com.tracker.sekon.sekontracker.GeoModule.LocationControllerImpl;

import java.util.ArrayList;

public class CurrentTrackViewModel extends ViewModel {

    private MutableLiveData<ArrayList<UnitTrackEntity>> trackListLiveData = new MutableLiveData<>();
    private ArrayList<UnitTrackEntity> trackList;

    private LocationControllerImpl locationController;


    public CurrentTrackViewModel() {
        locationController = LocationControllerImpl.getInstance();
        locationController.setGeoListener(new LocationController() {
            @Override
            public void getLocationData(GeoLocationData locationData) {
                System.out.println("пришли новые данные от контроллера");
            }
        });
    }

    public MutableLiveData<ArrayList<UnitTrackEntity>> getTrackListLiveData() {
        return trackListLiveData;
    }

    public void setUnitTrack(UnitTrackEntity track){
        trackList.add(track);
        trackListLiveData.postValue(trackList);
    }




}
