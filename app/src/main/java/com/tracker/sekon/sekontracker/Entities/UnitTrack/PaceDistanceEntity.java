package com.tracker.sekon.sekontracker.Entities.UnitTrack;

public class PaceDistanceEntity {

    private int paceDistance;
    private int averagePaceDistance;
    private int maxPaceDistance;

    public PaceDistanceEntity(int paceDistance, int averagePaceDistance, int maxPaceDistance) {
        this.paceDistance = paceDistance;
        this.averagePaceDistance = averagePaceDistance;
        this.maxPaceDistance = maxPaceDistance;
    }

    public int getPaceDistance() {
        return paceDistance;
    }

    public int getAveragePaceDistance() {
        return averagePaceDistance;
    }

    public int getMaxPaceDistance() {
        return maxPaceDistance;
    }
}
