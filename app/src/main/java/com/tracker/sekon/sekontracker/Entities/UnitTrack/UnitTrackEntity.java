package com.tracker.sekon.sekontracker.Entities.UnitTrack;

public class UnitTrackEntity {

    private int time;
    private int distance;
    private SpeedEntity speed;
    private int kkal;
    private PaceDistanceEntity paceDistance;
    private GeoPositionEntity geoPosition;

    public UnitTrackEntity(int time, int distance, int kkal, SpeedEntity speed, PaceDistanceEntity paceDistance, GeoPositionEntity geoPosition) {
        this.time = time;
        this.distance = distance;
        this.kkal = kkal;
        this.speed = speed;
        this.paceDistance = paceDistance;
        this.geoPosition = geoPosition;
    }

    public int getTime() {
        return time;
    }

    public int getDistance() {
        return distance;
    }

    public int getKkal() {
        return kkal;
    }

    public SpeedEntity getSpeed() {
        return speed;
    }

    public PaceDistanceEntity getPaceDistance() {
        return paceDistance;
    }

    public GeoPositionEntity getGeoPosition() {
        return geoPosition;
    }
}
