package com.tracker.sekon.sekontracker.GeoModule;

import com.tracker.sekon.sekontracker.Entities.UnitTrack.GeoPositionEntity;

public class GeoLocationData {

    private GeoPositionEntity geoPosition;
    private float speed;

    public GeoLocationData(GeoPositionEntity geoPosition, float speed) {
        this.geoPosition = geoPosition;
        this.speed = speed;
    }

    public GeoPositionEntity getGeoPosition() {
        return geoPosition;
    }

    public float getSpeed() {
        return speed;
    }

}
